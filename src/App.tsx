import {useEffect, useRef} from 'react'
import logo from './logo.svg';
import mapboxgl from 'mapbox-gl';

import 'mapbox-gl/dist/mapbox-gl.css';
import './App.css';

mapboxgl.accessToken = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA';

const Map = () => {
  const mapContainerRef = useRef(null)

  // Initialize map when component mounts
  useEffect(() => {
    const map = new mapboxgl.Map({
      container: mapContainerRef.current as unknown as HTMLElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [30.622714, 50.43337],
      zoom: 12
    })


    return () => map.remove()
  }, [])

  return <div className="map-container" ref={mapContainerRef} />
}


function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>

      <div>
        <div className="list">
          <div className="item">
            <p>ЖК Комфорт Таун, Соборности просп. (Воссоединения), 17, Днепровский р-н</p>
          </div>
          <div className="item">
            <p>ул. Маланюка (Степана Сагайдака) 10, кв 26</p>
          </div>
          <div className="item">
            <p>Надднепрянское шоссе, 2а, кв 56</p>
          </div>
        </div>
        <div className="map"><Map /></div>
      </div>
    </div>
  );
}

export default App;
